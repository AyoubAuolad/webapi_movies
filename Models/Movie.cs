﻿using System.ComponentModel.DataAnnotations;

namespace WEBAPP.Models
{
    public class Movie
    {
        public Movie()
        {
            this.Characters = new HashSet<Character>();
        }
        public int MovieId { get; set; }
        [MaxLength(30)]
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }

        //Navigation Properties
        public ICollection<Character> Characters { get; set; }
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
