﻿using System.ComponentModel.DataAnnotations;

namespace WEBAPP.Models
{
    public class Franchise
    {
        public int FranchiseId { get; set; }
        [MaxLength(30)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Description { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
