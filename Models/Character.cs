﻿using System.ComponentModel.DataAnnotations;

namespace WEBAPP.Models
{
    public class Character
    {
        public Character()
        {
            this.Movies = new HashSet<Movie>();
        }
        public int CharacterId { get; set; }
        [MaxLength(30)]
        public string Fullname { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
