﻿using Microsoft.EntityFrameworkCore;

namespace WEBAPP.Models
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// If models scaffolded the data will be seeded here
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeeds());

            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(c => c.Characters)
                .UsingEntity(j => j.HasData(
                    new { CharactersCharacterId = 1, MoviesMovieId = 1 },
                    new { CharactersCharacterId = 1, MoviesMovieId = 2 },
                    new { CharactersCharacterId = 1, MoviesMovieId = 4 },
                    new { CharactersCharacterId = 2, MoviesMovieId = 2 },
                    new { CharactersCharacterId = 2, MoviesMovieId = 3 },
                    new { CharactersCharacterId = 3, MoviesMovieId = 2 },
                    new { CharactersCharacterId = 3, MoviesMovieId = 3 },
                    new { CharactersCharacterId = 4, MoviesMovieId = 3 },
                    new { CharactersCharacterId = 4, MoviesMovieId = 4 }

                    ));

        }

        public DbSet<Movie> Movie { get; set; }
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }


    }
}
