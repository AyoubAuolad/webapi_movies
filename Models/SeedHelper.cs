﻿namespace WEBAPP.Models
{
    public class SeedHelper
    {
        public static IEnumerable<Movie> GetMovieSeeds()
        {
            var movieSeeds = new List<Movie>()
            {
                new Movie() { 
                    MovieId = 1,
                    FranchiseId = 1,
                    MovieTitle = "Superman The End",
                    Genre = "Action",
                    Trailer = "www.google.nl/Superman1",
                    Director = "Sup Erman",
                    Picture = "www.placeholder.com/image1.png",
                    ReleaseYear = 2022
                },
                new Movie() {
                    MovieId = 2,
                    FranchiseId = 2,
                    MovieTitle = "Spiderman Official",
                    Genre = "Thriller",
                    Trailer = "www.google.nl/Spiderman",
                    Director = "Spi Man",
                    Picture = "www.placeholder.com/image2.png",
                    ReleaseYear = 2009
                },
                new Movie() {
                    MovieId = 3,
                    FranchiseId = 2,
                    MovieTitle = "Batman: The End",
                    Genre = "Fight",
                    Trailer = "www.google.nl/Batman",
                    Director = "Battie Man",
                    Picture = "www.placeholder.com/image3.png",
                    ReleaseYear = 2021
                },
                new Movie() {
                    MovieId = 4,
                    FranchiseId = 1,
                    MovieTitle = "Hobbit 3",
                    Genre = "Action",
                    Trailer = "www.google.nl/Bilbo",
                    Director = "Misty Mountain",
                    Picture = "www.placeholder.com/image4.png",
                    ReleaseYear = 2018
                },
            };

            return movieSeeds;
        }

        public static IEnumerable<Character> GetCharacterSeeds()
        {
            var characterSeeds = new List<Character>()
            {
                new Character() {
                    CharacterId = 1,
                    Alias = "Superman",
                    Fullname = "Superman Hero",
                    Gender = "M",
                    Picture = "www.google.nl/supermanhero.png"
                },
               new Character() {
                    CharacterId = 2,
                    Alias = "Bat",
                    Fullname = "Batman",
                    Gender = "M",
                    Picture = "www.google.nl/batman.png"
                },
               new Character() {
                    CharacterId = 3,
                    Alias = "Batwoman",
                    Fullname = "Batwoman Hero",
                    Gender = "W",
                    Picture = "www.google.nl/batwoman.png"
                },
               new Character() {
                    CharacterId = 4,
                    Alias = "Spiderman",
                    Fullname = "Spiderman Hero",
                    Gender = "M",
                    Picture = "www.google.nl/spiderman.png"
                },
            };

            return characterSeeds;
        }

        public static IEnumerable<Franchise> GetFranchiseSeeds()
        {
            var franchiseSeeds = new List<Franchise>()
            {
                new Franchise() {
                    FranchiseId = 1,
                    Name = "Warner Bros",
                    Description = "Warner Bros is a populair franchise"
                },
               new Franchise() {
                    FranchiseId = 2,
                    Name = "Marvels",
                    Description = "Marvels is a populair franchise"
                },
              new Franchise() {
                    FranchiseId = 3,
                    Name = "Franchise 3",
                    Description = "Franchise 4 is not that populair but still a franchise"
                },
             new Franchise() {
                    FranchiseId = 4,
                    Name = "Franchise 4",
                    Description = "Description of Franchise 4"
                },
            };

            return franchiseSeeds;
        }
    }
}
