﻿namespace WEBAPP.DTO
{
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
