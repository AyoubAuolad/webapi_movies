﻿namespace WEBAPP.DTO
{
    public class MovieCharactersReadDTO
    {
        public int MovieId { get; set; }
        public ICollection<CharacterReadDTO> CharacterReadDTO { get; set; }

    }
}
