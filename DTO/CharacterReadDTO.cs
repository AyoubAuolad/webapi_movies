﻿using System.ComponentModel.DataAnnotations;

namespace WEBAPP.DTO
{
    public class CharacterReadDTO
    {
        public int CharacterId { get; set; }
        [MaxLength(30)]
        public string Fullname { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }

    }
}
