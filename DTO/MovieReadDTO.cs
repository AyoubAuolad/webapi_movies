﻿namespace WEBAPP.DTO
{
    public class MovieReadDTO
    {
        public int MovieId { get; set; }
        public int ReleaseYear { get; set; }
        public string MovieTitle { get; set; }
        public string Description { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }

    }
}
