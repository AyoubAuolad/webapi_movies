﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WEBAPP.DTO;
using WEBAPP.Models;

namespace WEBAPP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;  
        }

        /// <summary>
        /// Get all characters.
        /// </summary>
        /// <returns> List od character DTO's </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
          var characterList = await _context.Character.ToListAsync();

          var characterListDTO = _mapper.Map<List<CharacterReadDTO>>(characterList);

          return characterListDTO;
        }



        /// <summary>
        /// Get a specific character by ID
        /// </summary>
        /// <param name="id"> The ID to search a specific character</param>
        /// <returns> Character DTO object </returns>
        // GET: api/Characters/5
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Character.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            var characterDTO = _mapper.Map<CharacterReadDTO>(character);

            return characterDTO;
        }


        /// <summary>
        /// Update specific character by ID
        /// </summary>
        /// <param name="id">ID to navigate to the character I want to update</param>
        /// <param name="characterDTO">Given character DTO object</param>
        /// <returns>Updated character</returns>
        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDTO)
        {
            if (id != characterDTO.CharacterId)
            {
                return BadRequest();
            }

            var character = _mapper.Map<Character>(characterDTO);
            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        /// <summary>
        /// Creating a new character
        /// </summary>
        /// <param name="characterDTO">Character DTO as parameter</param>
        /// <returns>Newly created character with DTO instance</returns>
        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO characterDTO)
        {

            var character = _mapper.Map<Character>(characterDTO);

              if (character == null)
              {
                  return Problem("Entity set 'MovieDbContext.Character'  is null.");
              }

            _context.Character.Add(character);
            await _context.SaveChangesAsync();

            CharacterReadDTO newCharacterDTO = _mapper.Map<CharacterReadDTO>(character);

            return CreatedAtAction("GetCharacter", new { id = newCharacterDTO.CharacterId }, newCharacterDTO);
        }


        /// <summary>
        /// Delete specific character by ID
        /// </summary>
        /// <param name="id">Parameter ID to navigate</param>
        /// <returns>Deleted character</returns>
        // DELETE: api/Characters/5
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (_context.Character == null)
            {
                return NotFound();
            }
            var character = await _context.Character.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Character.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Check by ID if specific character exists.
        /// </summary>
        /// <param name="id">The ID to check for in the database</param>
        /// <returns>Boolean</returns>
        private bool CharacterExists(int id)
        {
            return (_context.Character?.Any(e => e.CharacterId == id)).GetValueOrDefault();
        }
    }
}
