﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WEBAPP.DTO;
using WEBAPP.Models;

namespace WEBAPP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get a list of franchise objects
        /// </summary>
        /// <returns>A list of franchise objects</returns>
        // GET: api/Franchises
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
          var franchiseList = await _context.Franchise.ToListAsync();

          if (franchiseList == null)
          {
              return NotFound();
          }

          var franchiseListDTO = _mapper.Map<List<FranchiseReadDTO>>(franchiseList);

          return franchiseListDTO;
        }

        /// <summary>
        /// Get a specific Franchise object by ID
        /// </summary>
        /// <param name="id">The ID to get a specific object</param>
        /// <returns>A franchise DTO object</returns>
        // GET: api/Franchises/5
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            var franchiseDTO = _mapper.Map<FranchiseReadDTO>(franchise);

            return franchiseDTO;
        }

        /// <summary>
        /// Update a specific Franchise object by ID
        /// </summary>
        /// <param name="id">The ID to search for specific franchise</param>
        /// <param name="franchiseDTO">Gets a DTO object of franchise as parameter</param>
        /// <returns>Updated franchise</returns>
        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseDTO)
        {
            if (id != franchiseDTO.FranchiseId)
            {
                return BadRequest();
            }

            var franchise = _mapper.Map<Character>(franchiseDTO);

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a new Franchise object
        /// </summary>
        /// <param name="franchisedto">Given Franchise DTO object</param>
        /// <returns>Franchise Domain object</returns>
        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchisedto)
        {
            var franchise = _mapper.Map<Franchise>(franchisedto);

            if (franchise == null)
            {
                return Problem("Entity set 'MovieDbContext.Franchise' is null.");
            }

            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();

            FranchiseReadDTO newFranchiseDTO = _mapper.Map<FranchiseReadDTO>(franchise);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = newFranchiseDTO.FranchiseId }, newFranchiseDTO);
        }

        /// <summary>
        /// Deletes a specific Franchise by ID
        /// </summary>
        /// <param name="id">Search for specific franchise by ID to delete</param>
        /// <returns>Nothing, deletes the specific franchise</returns>
        // DELETE: api/Franchises/5
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (_context.Franchise == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchise.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        
        /// <summary>
        /// Check if Franchise already exists in DB
        /// </summary>
        /// <param name="id">The ID of the franchise to check for</param>
        /// <returns>Boolean</returns>
        private bool FranchiseExists(int id)
        {
            return (_context.Franchise?.Any(e => e.FranchiseId == id)).GetValueOrDefault();
        }
    }
}
