﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WEBAPP.DTO;
using WEBAPP.Models;

namespace WEBAPP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get a list of all movie objects
        /// </summary>
        /// <returns>A list of movie objects</returns>
        // GET: api/Movies
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movieList = await _context.Movie.ToListAsync();

            if (movieList == null)
              {
                  return NotFound();
              }

            var movieListDTO = _mapper.Map<List<MovieReadDTO>>(movieList);
            return movieListDTO;
        }

        /// <summary>
        /// Get a specific movie by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>DTO object of single movie</returns>
        // GET: api/Movies/5
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            var movieDTO = _mapper.Map<MovieReadDTO>(movie);

            return movieDTO;
        }



        /// <summary>
        /// Get all characters from a movie by using the movie id
        /// </summary>
        /// <param name="id">Id that can be used to retrieve all characters of specific movie</param>
        /// <returns>Return a DTO of Movie with DTO's of Characters</returns>
        /// <response code="200">Successfully returns movie dto object</response>
        // GET: api/Movies/5/characters
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<MovieCharactersReadDTO>> GetCharactersMovie(int id)
        {
            var characters = await _context.Movie.Where(m => m.MovieId == id).SelectMany(m => m.Characters).ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }


            MovieCharactersReadDTO movieDTO = new()
            {
                MovieId = id,
                CharacterReadDTO = _mapper.Map<List<CharacterReadDTO>>(characters)
            };

            return movieDTO;
        }







        /// <summary>
        /// Update the characters of a movie by MovieId
        /// </summary>
        /// <param name="id">id of the specific movie you want to update</param>
        /// <param name="movieDTO">DTO object which consists of characterID's we want to have</param>
        /// <returns> Updated movie dto </returns>
        /// <response code="200">returns a movie dto object</response>
        // Put: api/Movies/5/updatecharacters
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}/UpdateCharacters")]
        public async Task<ActionResult<MovieReadDTO>> PutMovieCharacters(int id, MovieCharactersUpdateDTO movieDTO)
        {
            if (id != movieDTO.MovieId)
            {
                return BadRequest();
            }

            var movie = _context.Movie.Find(id);

            try
            {
                if (movie != null)
                {
                    foreach (int characterId in movieDTO.CharacterId)
                    {
                        var character = _context.Character.Find(characterId);
                        movie.Characters = await _context.Movie.Where(m => m.MovieId == id).SelectMany(m => m.Characters).ToListAsync();
                        movie.Characters.Add(character!);
                    }
                }
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            MovieReadDTO MovieReadDTO = _mapper.Map<MovieReadDTO>(movie);

            return MovieReadDTO;
        }









        /// <summary>
        /// Update a specific movie by ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieDTO">MOVIE DTO as parameter</param>
        /// <returns>A domain object of movie</returns>
        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieDTO)
        {
            if (id != movieDTO.MovieId)
            {
                return BadRequest();
            }

            var movie = _mapper.Map<Movie>(movieDTO);

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a new movie
        /// </summary>
        /// <param name="movieDTO"></param>
        /// <returns>A new movie object</returns>
        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movieDTO)
        {
            var movie = _mapper.Map<Movie>(movieDTO);

            if (movie == null)
            {
                return Problem("Entity set 'MovieDbContext.Movie'  is null.");
            }

            _context.Movie.Add(movie);
            await _context.SaveChangesAsync();

            MovieReadDTO newMovieReadDTO = _mapper.Map<MovieReadDTO>(movie);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = newMovieReadDTO.MovieId }, newMovieReadDTO);
        }

        /// <summary>
        /// Delete specific movie by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deleted movie</returns>
        // DELETE: api/Movies/5
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (_context.Movie == null)
            {
                return NotFound();
            }
            var movie = await _context.Movie.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Check if movie already exists in database by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Boolean</returns>
        private bool MovieExists(int id)
        {
            return (_context.Movie?.Any(e => e.MovieId == id)).GetValueOrDefault();
        }
    }
}
