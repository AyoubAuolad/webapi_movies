[![license](https://img.shields.io/badge/License-MIT-green.svg)](LICENSE)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## WEB API ASP ENTITY FRAMEWORK

For this project we had to work on developing a web api with proper documentation.
A complete list of all requirements is given in the assignment PDF

Created by Ayoub Auolad ali.

## Table of Contents

- [Security](#security)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Security

I make use of the SQL dependency method when executing database communication functions. This provides a secure communication and can prevent an SQL injection attack.



## Install
Download and install: 
* [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)
* [.Net 6.0 or later](https://dotnet.microsoft.com/en-us/download/dotnet)

Clone the repository using:

```
git clone git@gitlab.com:/AyoubAuolad/webapi_movies

## Usage

## Maintainers

[@AyoubAuolad](https://gitlab.com/AyoubAuolad)\

## Contributing

PRs accepted.

Small note: If editing the Readme, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

[MIT � Ayoub Auolad ali](../LICENSE)
