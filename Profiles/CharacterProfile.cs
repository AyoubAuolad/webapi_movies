﻿using AutoMapper;
using WEBAPP.DTO;
using WEBAPP.Models;

namespace WEBAPP.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            //Mapping DTO's and DOMAIN entities on creation, read and update.
            CreateMap<Character, CharacterReadDTO>();
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();

        }
    }
}
