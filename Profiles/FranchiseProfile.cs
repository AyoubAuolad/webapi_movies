﻿using AutoMapper;
using WEBAPP.DTO;
using WEBAPP.Models;

namespace WEBAPP.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            //Mapping DTO's and DOMAIN entities on creation, read and update.
            CreateMap<Franchise, FranchiseReadDTO>();
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}
