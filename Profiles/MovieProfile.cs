﻿using AutoMapper;
using WEBAPP.DTO;
using WEBAPP.Models;

namespace WEBAPP.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            //Mapping DTO's and DOMAIN entities on creation, read and update.
            CreateMap<Movie, MovieReadDTO>();
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieEditDTO, Movie>();

        }
    }
}
